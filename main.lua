package.path        = package.path..";/usr/lib64/lua/luarocks/share/lua/5.1/?.lua;/usr/lib64/lua/luarocks/share/lua/5.1/?/?.lua;./lua/?.lua;./?.lua";
local MCASTMUX_HOME = os.getenv("MCASTMUX_HOME");
local lscript_path  = MCASTMUX_HOME.."/lua"
local JSONf         = assert(loadfile(lscript_path.."/JSON.lua"));
local JSONf         = JSONf()
local zapi          = require("zapi");
local soap          = require("CMDBuildSOAPAPI")
local LSHW,LSHWResult,path,PATTERN = {},{},{}
local LocationsIP   = require("locationsIP")
local LSHWRES={}
LSHW={
  ["ABOUTME"]={
    ["_MODULE"]         = "LSHW-CMDBuild",
    ["_VERSION"]        = "LSHW-CMDBuild v0.1.0-rc1",
    ["_AUTHOR"]         = "Alexandr Mikhailenko (he also) FlashHacker",
    ["_URL"]            = "https://bitbucket.org/enlab/LSHW-CMDBuild",
    ["_MAIL"]           = "flashhacker1988@gmail.com",
    ["_COPYRIGHT"]      = "Design Bureau of Industrial Communications LTD, 2015. All rights reserved.",
    ["_LICENSE"]        = [[
      MIT LICENSE
      Copyright (c) 2015 Mikhailenko Alexandr Konstantinovich (a.k.a) Alex M.A.K
      Permission is hereby granted, free of charge, to any person obtaining a
      copy of this software and associated documentation files (the
      "Software"), to deal in the Software without restriction, including
      without limitation the rights to use, copy, modify, merge, publish,
      distribute, sublicense, and/or sell copies of the Software, and to
      permit persons to whom the Software is furnished to do so, subject to
      the following conditions:
      The above copyright notice and this permission notice shall be included
      in all copies or substantial portions of the Software.
      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
      OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
      MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
      IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
      CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
      TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ]],
    ["_DESCRIPTION"]    = "Added LSHW (Linux) information to CMDBuild"
  },
  ["SETTINGS"]={
    ["WS_URL"]        = "http://192.168.56.10:8080/cmdbuild/services/soap/Webservices",
    ["WS_URL_Private"]= "http://192.168.56.10:8080/cmdbuild/services/soap/Private",
    ["WS_ADMIN"]      = "admin",
    ["WS_PASS"]       = "3$rFvGz/uJm",
    ["PATHXML"]       = "xml",
  },

  --- Function names preparation for comparison
  -- @param value -- string
  -- @usage LSHW["locName"](module)
  -- @return string without special characters
  ["locName"]=function(value)
    return value:gsub("-",""):gsub("[(]",""):gsub("[)]","");
  end,

  --- The function for checking the module presence
  -- @param module -- name module for include
  -- @param VALUE - the serial number of the automation object
  -- @param ARRAY - table - the function result LocationsIP["ParseXml"]()
  -- @usage LSHW["LOADREQUIRE"](module)
  -- @return nil
  ["LOADREQUIRE"]=function(module)
    local function requiref(module) require(module) end
    res = pcall(requiref, module)
    if not(res) then 
      print(string.format("Module %s: ERROR LOAD MODULE: %s", arg[0], module))
    else
      print(string.format("Module %s: LOAD MODULE: %s - %s", arg[0], module, "done!")) 
    end
    return nil
  end,

  --- The function for check exists a map section EqProd
  -- @param KEY -- filter name (ex.: "SN")
  -- @param VALUE - the serial number of the automation object
  -- @param ARRAY - table - the function result LocationsIP["ParseXml"]()
  -- @usage LSHW["ExistEqProdCard"](array,SN,VERBOSE)
  -- @return nil
  ["ExistEqProdCard"]=function(KEY,VALUE,ARRAY)
    local xmltext=soap.getCardList(LSHW["SETTINGS"]["WS_ADMIN"], LSHW["SETTINGS"]["WS_PASS"], 
      "EqProd", nil, {["name"]=KEY, ["operator"]="EQUALS", ["value"]=VALUE})
    local xmlresp=soap.retriveMessage(soap.Send(LSHW["SETTINGS"]["WS_URL"], xmltext))
    local duplicateID=xml.eval(xmlresp)
    if duplicateID[1]:find("ns2:id")~=nil and duplicateID[1]:find("ns2:id")[1]~=nil then
      LSHW["UpdateEqProdCard"](ARRAY,duplicateID[1]:find("ns2:id")[1],1)
    end
    return nil
  end,

  --- The function to update a map section EqProd
  -- @param array -- table - the function result LocationsIP["ParseXml"]()
  -- @param SN - the serial number of the automation object
  -- @param VERBOSE - verbose mode
  -- @usage LSHW["UpdateEqProdCard"](array,SN,VERBOSE)
  -- @return msg
  ["UpdateEqProdCard"]=function(array,SN,VERBOSE)
    local xmltext=soap.updateCard(LSHW["SETTINGS"]["WS_ADMIN"], LSHW["SETTINGS"]["WS_PASS"], "EqProd", SN,array)
    local xmlresp=soap.retriveMessage(soap.Send(LSHW["SETTINGS"]["WS_URL"], xmltext))
    local xmlError=xml.eval(xmlresp)
    if xmlError~=nil and xmlError:find("ns2:return")~=nil and VERBOSE==1 then
      print("Update EqProd card, CID: "..SN.." response: "..xmlError:find("ns2:return")[1])
    end
    return true
  end,

  --- The function to create a map section EqProd
  -- @param array -- table - the function result LocationsIP["ParseXml"]()
  -- @param VERBOSE - verbose mode
  -- @usage LSHW["CreateEqProdCard"](array,VERBOSE)
  -- @return msg
  ["CreateEqProdCard"]=function(array,VERBOSE)
    local xmltext=soap.createCard(LSHW["SETTINGS"]["WS_ADMIN"], LSHW["SETTINGS"]["WS_PASS"], "EqProd", array)
    local xmlresp=soap.retriveMessage(soap.Send(LSHW["SETTINGS"]["WS_URL"], xmltext))
    local xmlError=xml.eval(xmltext)
    if xmlresp~=nil then
      local xmltab=xml.eval(xmlresp)
      if xmltab~=nil and xmltab:find("ns2:return")~=nil and VERBOSE==1 then
        print("Create EqProd card, CID: "..xmltab:find("ns2:return")[1])
      else
        table.foreach(array,function(k,v) if k=="SN" then LSHW["ExistEqProdCard"](k,v,array,1,1) end end)
      end
    else
      error("Resp error: data nil")
    end
    return nil
  end,

  --- The function to create a map in the section AddressIPv4
  -- @param ipaddress -- ip address for add
  -- @usage LSHW["CreateAddressIPv4Card"](ipaddress)
  -- @return msg
  ["CreateAddressIPv4Card"]=function(ipaddress)
    local xmltext=soap.createCard(LSHW["SETTINGS"]["WS_ADMIN"], LSHW["SETTINGS"]["WS_PASS"],"AddressesIPv4", {["Address"]=ipaddress.."/24"})
    local xmlresp=soap.retriveMessage(soap.Send(LSHW["SETTINGS"]["WS_URL"], xmltext))
    local xmlError=xml.eval(xmlresp)
    if xmlresp~=nil then
      if xmlError~=nil and xmlError:find("ns2:return")~=nil then
        print("Create AddressIPv4Card card, CID: "..xmlError:find("ns2:return")[1])
      end
    else
      error("Resp error: data nil")
    end
  end,

  --- The function of producing the ID pattern from section Locations
  -- @param IP -- address for comparison
  -- @param PATTERN -- pattern - the function result LocationsIP["response"]()
  -- @usage LSHW["GetLocation"](IP,PATTERN)
  -- @return Id
  ["GetLocation"]=function(IP,PATTERN)
    local SUM, SEM, result, response={},0,0,0
    local xmltext=soap.getCardList(LSHW["SETTINGS"]["WS_ADMIN"], LSHW["SETTINGS"]["WS_PASS"], "Locations");
    local xmlresp=soap.retriveMessage(soap.Send(LSHW["SETTINGS"]["WS_URL"], xmltext))
    if xmlresp~=nil then 
      result=xml.eval(xmlresp) 
    end
    for ii=1,table.maxn(result[1][1][1]) do
      for kk=1,table.maxn(result[1][1][1][ii]) do
        if result[1][1][1][ii][kk]:find("ns2:name")~=nil and result[1][1][1][ii][kk]:find("ns2:value")~=nil then
          if result[1][1][1][ii][kk]:find("ns2:name")[1]=="Description" then
            if PATTERN~=nil and string.match(LSHW["locName"](result[1][1][1][ii][kk]:find("ns2:value")[1]), LSHW["locName"](PATTERN)) then
              table.insert(SUM,result[1][1][1][ii]:find("ns2:id")[1])
              if result[1][1][1][ii]:find("ns2:id")[1]==nil then
                SEM=0
              end
            end
          end
        end
      end
    end
    if #SUM==1 then
      response=SUM[1]
    elseif #SUM==2 then
      LSHW["String2File"](".","information.log",IP.."::"..PATTERN.."::"..#SUM)
    elseif PATTERN~=nil then
      LSHW["String2File"](".","information.log",IP.."::"..PATTERN.."::"..SEM)
    end
    return response
  end,

  --- Function for parse XML table
  -- @param ID -- path to save file
  -- @param ArrayXML -- name file to save
  -- @param LocationID     --  the function result LSHW["GetLocation"]()
  -- @param VERBOSE -- verbose mode
  -- @param DEBUG -- debug mode
  -- @usage LSHW["ParseXml"](ID,ArrayXML,LocationID,VERBOSE,DEBUG)
  -- @return table
  ["ParseXml"]=function(ID,ArrayXML,LocationID,VERBOSE,DEBUG)
    local flags={}
    local tempLSHWResult={}
    if ArrayXML~=nil then IPRTS=ArrayXML end
    if LocationID~=nil then tempLSHWResult["Location"]=LocationID end
    LSHW["CreateAddressIPv4Card"](IPRTS)
    table.foreach(LSHW["FindNodeXML"](xml.eval(LSHW["File2String"](ArrayXML)), "node"), function(k,v)
      for _, node in pairs(v) do
        if node.TAG~=nil then
          if node:find(node[node.TAG])[1]~=nil then
            if node:find("description")~=nil and node:find("serial")~=nil then
              if not flags[IPRTS] then
                LSHWResult[IPRTS]={}
                tempLSHWResult["Description"]=ArrayXML
                flags[IPRTS]=true
              end
              if string.len(ID)<2 then
                for k,v in pairs({'CPUSN', 'DT1SN', 'DT2SN', 'PSU1SN', 'PSU2SN'}) do
                  tempLSHWResult[v]=string.format("UNK-000%s",ID)
                end
              else
                for k,v in pairs({'CPUSN', 'DT1SN', 'DT2SN', 'PSU1SN', 'PSU2SN'}) do
                  tempLSHWResult[v]=string.format("UNK-00%s",ID)
                end
              end
              if node:find("description")[1]=="DIMM Synchronous 1333 MHz (0.8 ns)" 
                or node:find("description")[1]=="DIMM DDR3 Synchronous 1333 MHz (0.8 ns)" and node:find("slot")~=nil then
                  if node:find("slot")[1]=="DIMM_A1" or node:find("slot")[1]=="ChannelA-DIMM0" then
                    tempLSHWResult["RAM1SN"]=node:find("serial")[1]
                  end
                  if node:find("slot")[1]=="DIMM_B1" or node:find("slot")[1]=="ChannelB-DIMM0" then
                    tempLSHWResult["RAM2SN"]=node:find("serial")[1]
                  end
              elseif node:find("description")[1]=="ATA Disk" and node:find("businfo")~=nil then
                if node:find("businfo")[1]=="scsi@0:0.0.0" or node:find("businfo")[1]=="scsi@2:0.0.0" then
                  tempLSHWResult["HDD1SN"]=node:find("serial")[1]
                elseif node:find("businfo")[1]=="scsi@1:0.0.0" or node:find("businfo")[1]=="scsi@3:0.0.0" then
                  tempLSHWResult["HDD2SN"]=node:find("serial")[1]
                end
              elseif node:find("logicalname")~=nil and node:find("logicalname")[1]=="eth0" and node:find("serial")~=nil then
                tempLSHWResult["NIC"]=node:find("serial")[1]
              elseif node:find("description")[1]=="Motherboard" and node:find("serial")~=nil then
                tempLSHWResult["MBSN"]=node:find("serial")[1]
              elseif node:find("logicalname")~=nil and node:find("logicalname")[1]=="eth1" and node:find("serial")~=nil then
                tempLSHWResult["SN"]=node:find("serial")[1]
              end
              LSHWResult[IPRTS]=tempLSHWResult
            end
          end
        end
      end
    end)
    return LSHWResult
  end,

  --- The function mcc.of a general purpose data recording file
  -- @param location -- path to save file
  -- @param filename -- name file to save
  -- @param data     -- any string value
  -- @usage String2File(location, filename, "Welcome\nBla-bla-bla")
  -- @usage String2File(location, filename, unpack(array))
  -- @return state "1" or "0" and loggind data to file
  ["String2File"]=function(location, filename, data)
    if location and filename and data ~= nil then
      local path = (location or lscript_path) .. "/" .. filename -- определяем путь и имя файла
      file = assert(io.open(path, "a"))                   -- открываем файл в режиме записи
      io.output(file)                                     -- указываем режим вход или выход
      io.write(data, "\n")                                -- записываем наши данные в файл
      io.close(file)                                      -- закрываем файл чтобы не висел в памяти
      return 0
    else
      print( "variable location and filename/data are empty or not correctly filled in")
      return 1
    end
  end,

  --- The function mcc.of a general purpose recording file in of string
  -- @param location -- path to save file
  -- @param filename -- name file to save
  -- @usage fileToString(location, filename)
  -- @usage fileToString("/home/", "filename.json")
  -- @return json_string or 1
  ["File2String"]=function(filename)
    if filename ~= nil then
      local path = "xml/"..filename..".xml"
      local jsonConfigFile = assert(io.open(path, "r"))
      local stringFile = jsonConfigFile:read "*a"
      jsonConfigFile:close()
      return stringFile
    else
      print(string.format("I can not convert the file: %s into a string",filename))
      return 1
    end
  end, 

  --- Function to search XML files and preparation of IP addresses
  -- @param directory -- path to search file
  -- @return string (IP)
  ["FindXml"]=function(directory)
    local i, t, popen = 0, {}, io.popen
    for filename in popen('ls -a "'..directory..'"/*.xml'):lines() do
      i = i + 1
      t[i] = string.format("%d.%d.%d.%d", string.match(filename, "(%d+).(%d+).(%d+).(%d+)"))
    end
    return t
  end,  

  --- The search function on the node filters
  -- @param xmltable -- xml table or file
  -- @param tag -- filter
  -- @result -- null or your name
  -- @return result (table)
  ["FindNodeXML"]=function(xmltable, tag, results)
    local results = results or {}
    if xmltable~=nil then
      if xmltable[xml.TAG] == tag then 
        table.insert(results, xmltable) 
      end
      for k, v in pairs(xmltable) do
        if type(v) == "table" then 
          LSHW["FindNodeXML"](v, tag, results) 
        end
      end
    end
    return results
  end,

  --- Start ;-)
  -- @param dir - dir name for find XML file
  -- @param tag -- filter
  -- @result -- null or your name
  -- @return result (table)
  ["StartProc"]=function(dir)
    LSHW["LOADREQUIRE"]("LuaXml")
    LSHW["LOADREQUIRE"]("LuaXML")
    local IP=LSHW["FindXml"](dir)
    for i=1, #IP do
      MATCHIP=string.format("%d.%d.%d.%d", string.match(IP[i], "(%d+).(%d+).(%d+).(%d+)"))
      PATTERN=LocationsIP["response"](MATCHIP)
      RESULTParse=LSHW["ParseXml"](i, IP[i], LSHW["GetLocation"](MATCHIP, PATTERN), 1, 0)
    end
    table.foreach(RESULTParse, function(k,v) 
      LSHW["CreateEqProdCard"](v, 1) 
    end)
  end
}

return LSHW["StartProc"]("xml")